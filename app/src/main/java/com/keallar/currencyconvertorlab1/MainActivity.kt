package com.keallar.currencyconvertorlab1

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import android.widget.Toast
import androidx.activity.ComponentActivity
import org.json.JSONObject
import kotlin.concurrent.thread

class MainActivity : ComponentActivity() {
    private val utils: Utils = Utils(this)

    private lateinit var fromSp: Spinner
    private lateinit var toSp: Spinner
    private lateinit var textAmount: EditText
    private lateinit var doneBtn: Button

    private val currencies: Map<String, Currency> by lazy {
        resources.openRawResource(R.raw.currencies).use { stream ->
            JSONObject(String(stream.readBytes())).toMap().mapValues {
                val value = it.value as Map<*, *>
                Currency(it.key, value["name"] as String, value["country"] as String)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fromSp = findViewById(R.id.spFrom)
        toSp = findViewById(R.id.spTo)
        textAmount = findViewById(R.id.amount)
        doneBtn = findViewById(R.id.done)

        val from = currencies.keys.toTypedArray().map { it.uppercase() }
        val adFrom: ArrayAdapter<String> = ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, from)
        fromSp.adapter = adFrom

        val to = currencies.keys.toTypedArray().map { it.uppercase() }
        val adTo: ArrayAdapter<String> = ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, to)
        toSp.adapter = adTo

        doneBtn.setOnClickListener {
            var tot: Double
            if (textAmount.text.isNullOrBlank()) {
                Toast.makeText(applicationContext, "Print amount!!!", Toast.LENGTH_LONG).show()
            } else {
                val amount: Double = textAmount.text.toString().toDouble()
                val fromCur: String = fromSp.selectedItem.toString().lowercase()
                val toCur: String = toSp.selectedItem.toString().lowercase()
                thread {
                    val rate: Double? = utils.getRate(fromCur, toCur)
                    println("rate: $rate")
                    Handler(Looper.getMainLooper()).post {
                        if (rate == null) {
                            Toast.makeText(applicationContext, "Something went wrong", Toast.LENGTH_LONG).show()
                        } else {
                            tot = rate * amount
                            Toast.makeText(applicationContext, tot.toString(), Toast.LENGTH_LONG).show()
                        }
                    }
                }
            }
        }
    }
}