package com.keallar.currencyconvertorlab1

import android.content.Context
import android.util.Log
import org.json.JSONArray
import org.json.JSONObject
import java.net.URL

class Utils(private val context: Context) {
    fun getRate(base: String, quote: String): Double? {
        val url = context.getString(R.string.currency_rate_url, base, quote)
        println("url: $url")
        return try {
            val data = URL(url).readText()
            val json = JSONObject(data)
            json.getDouble(quote)
        } catch (e: Exception) {
            Log.d("CurrencyManager", "getRate Exception", e)
            null
        }
    }
}

fun JSONObject.toMap(): Map<String, *> = keys().asSequence().associateWith {
    when (val value = get(it)) {
        is JSONObject -> value.toMap()
        is JSONArray -> value.toList()
        else -> value
    }
}

fun JSONArray.toList(): List<*> = (0 until length()).asSequence().map {
    when (val value = get(it)) {
        is JSONObject -> value.toMap()
        is JSONArray -> value.toList()
        else -> value
    }
}.toList()