package com.keallar.currencyconvertorlab1

data class Currency(val code: String, val name: String, val countryCode: String)
